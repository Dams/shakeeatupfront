import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { HttpClient, HttpHeaders } from "@angular/common/http"
import { Storage } from '@ionic/storage';


/*
  Generated class for the MainProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MainProvider {


  /* Déclaration des variables */
  private base_url = "http://localhost:1337/";


  constructor(private myHttp: Http, private storage: Storage) {
  }

  public get(url) {
    let ctx = this;
    return new Promise(function (resolve, reject) {
      ctx.storage.get('token').then((val) => {

        let headers = new Headers();
        headers.append('Authorization', val);

        let call = ctx.myHttp.get(ctx.base_url + url, { headers: headers });

        call.subscribe(
          (result) => {
            resolve(result.json());

          }, (error) => {
            resolve(error.json());
          }
        );
      });
    })
  }

  public post(url, body) {
    let ctx = this;
    return new Promise(function (resolve, reject) {
      ctx.storage.get('token').then((val) => {

        let headers = new Headers();
        headers.append('Authorization', val);

        let call = ctx.myHttp.post(ctx.base_url + url, body, { headers: headers });

        call.subscribe(
          (result) => {
            resolve(result.json());

          }, (error) => {
            resolve(error.json());
          }
        );
      });
    })
  }



}
