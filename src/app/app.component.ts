import { ProgrammeTrainPage } from './../pages/programme-train/programme-train';
import { SeanceTrainPage } from './../pages/seance-train/seance-train';
import { Component, ViewChild } from "@angular/core";
import { Platform, Nav } from "ionic-angular";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//import { Keyboard } from '@ionic-native/keyboard';

import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { LocalWeatherPage } from "../pages/local-weather/local-weather";
import { TripsPage } from "../pages/trips/trips";
import { ProfilPage } from "../pages/profil/profil";
import { CreateProgrammePage } from "../pages/create-programme/create-programme";
import { CreateSceancePage } from "../pages/create-sceance/create-sceance";
import { MesProgrammesPage } from "../pages/mes-programmes/mes-programmes";
import { RepasPage } from "../pages/repas/repas";
import { SeancePage } from "../pages/seance/seance";

import { Storage } from '@ionic/storage';
import { GraphPage } from "../pages/graph/graph";
import { ModifRepasPage } from "../pages/modif-repas/modif-repas";
import { MonCalendrierPage } from "../pages/mon-calendrier/mon-calendrier";
import { ExercicePage } from "../pages/exercice/exercice";

export interface MenuItem {
  title: string;
  component: any;
  icon: string;
  user: number
}

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  appMenuItems: Array<MenuItem>;
  userData: {
    "nom": "",
    "prenom": ""
  };
  name: any;
  fonction: string;
  role: number;


  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    //public keyboard: Keyboard,
    public storage: Storage
  ) {
    this.initializeApp();

    this.appMenuItems = [
      { title: 'Accueil', component: TripsPage, icon: 'home', user: 0 }, //HomePage
      //{ title: 'Tous les programmes', component: TripsPage, icon: 'partly-sunny', user: 1 },
      { title: 'Mes Programmes', component: MesProgrammesPage, icon: 'partly-sunny', user: 1 },
      { title: 'Mes Programmes Diet', component: MesProgrammesPage, icon: 'partly-sunny', user: 10 },
      { title: 'Mes Séances Diet', component: SeancePage, icon: 'partly-sunny', user: 10 },
      { title: 'Mes Repas', component: RepasPage, icon: 'partly-sunny', user: 10 },
      { title: 'Mes Programmes Training', component: ProgrammeTrainPage, icon: 'partly-sunny', user: 10 },
      { title: 'Mes Séances Training', component: SeanceTrainPage, icon: 'partly-sunny', user: 10 },
      { title: 'Mes Exercices', component: ExercicePage, icon: 'partly-sunny', user: 10 },
      //{ title: 'Planning', component: MonCalendrierPage, icon: 'md-calendar', user : 1},
      { title: 'Graphique d\'évolution', component: GraphPage, icon: 'stats', user: 1 },
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.

      //*** Control Splash Screen
      // this.splashScreen.show();
      // this.splashScreen.hide();

      //*** Control Status Bar
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);

      //*** Control Keyboard
      //this.keyboard.disableScroll(true);
    });
    let ctx = this;
    let interData = setInterval(function () {
      ctx.storage.get('dataUser').then((val) => {

        ctx.userData = val;
        ctx.name = val.prenom + " " + val.nom;
        ctx.role = val.role;
        if (val.role == 1)
          ctx.fonction = "Utilisateur"
        else
          ctx.fonction = "Coach"

        if ("id" in ctx.userData) {
          if (ctx.rootPage == LoginPage)
            ctx.rootPage = TripsPage; //HomePage
          //clearInterval(interData);
        }
      });
    }, 1000);


  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout() {
    this.storage.clear();
    this.nav.setRoot(LoginPage);
  }

  editProfil() {
    this.nav.push(ProfilPage);
  }

}
