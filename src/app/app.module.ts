import { DeroulementRepasPage } from './../pages/deroulement-repas/deroulement-repas';
import { ProgrammeTrainPage } from './../pages/programme-train/programme-train';
import { SeanceTrainDetailPage } from './../pages/seance-train-detail/seance-train-detail';
import { SeanceTrainPage } from './../pages/seance-train/seance-train';
import { NgModule } from "@angular/core";
import { IonicApp, IonicModule } from "ionic-angular";
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { CalendarModule } from "ion2-calendar";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//import { Keyboard } from '@ionic-native/keyboard';

import { ActivityService } from "../services/activity-service";
import { TripService } from "../services/trip-service";
import { WeatherProvider } from "../services/weather";

import { MyApp } from "./app.component";

import { SettingsPage } from "../pages/settings/settings";
import { CheckoutTripPage } from "../pages/checkout-trip/checkout-trip";
import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { NotificationsPage } from "../pages/notifications/notifications";
import { RegisterPage } from "../pages/register/register";
import { RegisterCoachPage } from "../pages/register-coach/register-coach";
import { SearchLocationPage } from "../pages/search-location/search-location";
import { TripDetailPage } from "../pages/trip-detail/trip-detail";
import { TripsPage } from "../pages/trips/trips";
import { LocalWeatherPage } from "../pages/local-weather/local-weather";
import { ProfilPage } from "../pages/profil/profil";
import { MesProgrammesPage } from "../pages/mes-programmes/mes-programmes";
import { SeancePage } from "../pages/seance/seance";
import { SeanceDetailPage } from "../pages/seance-detail/seance-detail";
import { HttpModule } from "@angular/http";
import { MainProvider } from '../providers/provider';
import { CreateProgrammePage } from "../pages/create-programme/create-programme";
import { UpdateProgrammePage } from "../pages/update-programme/update-programme";
import { CreateSceancePage } from "../pages/create-sceance/create-sceance";
import { RepasPage } from "../pages/repas/repas";
import { GraphPage } from "../pages/graph/graph";
import { ModifRepasPage } from "../pages/modif-repas/modif-repas";
import { CreateRepasPage } from "../pages/create-repas/create-repas";
import { MonCalendrierPage } from "../pages/mon-calendrier/mon-calendrier";
import { CreateProgrammeTrainPage } from "../pages/create-programme-train/create-programme-train";
import { CreateExercicePage } from "../pages/create-exercice/create-exercice";
import { CreateSeanceTrainPage } from "../pages/create-seance-train/create-seance-train";
import { ModifSeanceTrainPage } from "../pages/modif-seance-train/modif-seance-train";
import { ModifExercicePage } from "../pages/modif-exercice/modif-exercice";
import { UpdateProgrammeTrainPage } from "../pages/update-programme-train/update-programme-train";
import { ExercicePage } from "../pages/exercice/exercice";
import { DeroulementTrainingPage } from '../pages/deroulement-training/deroulement-training';



// import services
// end import services
// end import services

// import pages
// end import pages

@NgModule({
  declarations: [
    MyApp,
    SettingsPage,
    CheckoutTripPage,
    HomePage,
    ProfilPage,
    LoginPage,
    LocalWeatherPage,
    CreateProgrammePage,
    UpdateProgrammePage,
    CreateSceancePage,
    ModifRepasPage,
    RepasPage,
    NotificationsPage,
    RegisterPage,
    SearchLocationPage,
    TripDetailPage,
    TripsPage,
    MesProgrammesPage,
    SeancePage,
    SeanceDetailPage,
    RegisterCoachPage,
    GraphPage,
    CreateRepasPage,
    MonCalendrierPage,
    CreateProgrammeTrainPage,
    CreateExercicePage,
    CreateSeanceTrainPage,
    ModifSeanceTrainPage,
    ModifExercicePage,
    UpdateProgrammeTrainPage,
    ExercicePage,
    SeanceTrainPage,
    SeanceTrainDetailPage,
    ProgrammeTrainPage,
    DeroulementTrainingPage,
    DeroulementRepasPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    CalendarModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    }),
    IonicStorageModule.forRoot({
      name: '__ionic3_start_theme',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SettingsPage,
    CheckoutTripPage,
    ProfilPage,
    HomePage,
    LoginPage,
    LocalWeatherPage,
    CreateProgrammePage,
    UpdateProgrammePage,
    CreateSceancePage,
    RepasPage,
    ModifRepasPage,
    NotificationsPage,
    RegisterPage,
    SearchLocationPage,
    TripDetailPage,
    TripsPage,
    MesProgrammesPage,
    SeancePage,
    SeanceDetailPage,
    RegisterCoachPage,
    GraphPage,
    CreateRepasPage,
    MonCalendrierPage,
    CreateProgrammeTrainPage,
    CreateExercicePage,
    CreateSeanceTrainPage,
    ModifSeanceTrainPage,
    ModifExercicePage,
    UpdateProgrammeTrainPage,
    ExercicePage,
    SeanceTrainPage,
    SeanceTrainDetailPage,
    ProgrammeTrainPage,
    DeroulementTrainingPage,
    DeroulementRepasPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    //Keyboard,
    ActivityService,
    TripService,
    WeatherProvider,
    MainProvider
  ]
})

export class AppModule {

}
