export let TRIPS = [
    {
        id: 1,
        name: "Ready for the Beach",
        type: "Perte de poids",
        price_adult: 0,
        price_child: 0,
        time: "30 jours",
        free_cancellation: 1,
        electric_voucher: 1,
        sub_name: "English Commentary Tour",
        thumb: "assets/img/trip/thumb/trip_1.jpg",
        description: "Si vous voulez vous remettre en forme pour l'été et obtenir le fameux Summer Body, ce programme de 30j est fait pour vous !",
        images: [
            "assets/img/trip/thumb/trip_5.jpg",
            "assets/img/trip/thumb/trip_6.jpg",
            "assets/img/trip/thumb/trip_7.jpg",
            "assets/img/trip/thumb/trip_8.jpg",
        ],
        highlights: [
            "Accessible à tous",
            "Gratuit",
            "Fais par des professionnels de la nutrition",
            "Résultats rapides",
            "Satisfaction garantie",
        ]
    },
    {
        id: 2,
        name: "Redemption of the skinny guys",
        type: "Prise de masse",
        price_adult: 5,
        time: "60 jours",
        free_cancellation: 1,
        electric_voucher: 1,
        sub_name: "English Commentary Tour",
        thumb: "assets/img/trip/thumb/trip_2.jpg",
        description: "Si tu as été maigre toute ta vie et que tu veux changer, commence dès maintenant avec ce programme qui va te métamorphoser ! Le changement commence ici.",
        images: [
            "assets/img/trip/thumb/trip_5.jpg",
            "assets/img/trip/thumb/trip_6.jpg",
            "assets/img/trip/thumb/trip_7.jpg",
            "assets/img/trip/thumb/trip_8.jpg",
        ],
        highlights: [
            "Accessible à tous",
            "Gratuit",
            "Fais par des professionnels de la nutrition",
            "Résultats rapides",
            "Satisfaction garantie",
        ]
    }
]
