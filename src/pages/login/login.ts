import { Component } from "@angular/core";
import { NavController, AlertController, ToastController, MenuController } from "ionic-angular";
import { HomePage } from "../home/home";
import { RegisterPage } from "../register/register";
import { RegisterCoachPage } from "../register-coach/register-coach";
import { MainProvider } from "../../providers/provider";
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { TripsPage } from "../trips/trips";


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  constructor(public nav: NavController, public forgotCtrl: AlertController, public menu: MenuController, public toastCtrl: ToastController, public myProvider: MainProvider, private storage: Storage) {
    this.menu.swipeEnable(false);
  }
  loginForm: FormGroup;

  ngOnInit() {
    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.loginForm = new FormGroup({
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(12)]),
      email: new FormControl('', [Validators.required, Validators.pattern(EMAILPATTERN)]),
    });
  }


  // go to register page
  register() {
    this.nav.setRoot(RegisterPage);
  }
  registerCoach() {
    this.nav.setRoot(RegisterCoachPage)
  }

  // login and go to home page
  async login(data) {
    let login = await this.myProvider.post("auth/login", data.value);
    /*this.storage.set("dataUser", login["resultat"]);
    this.nav.setRoot(HomePage);*/
    console.log(login);
    if (login["resultat"] != "Mauvaise combinaison") {
      this.storage.set("dataUser", login["resultat"])
      this.nav.setRoot(TripsPage);
    }
    else {
      this.presentAlert("Mauvais identifiant et/ou mot de passe");
    }
  }

  presentAlert(text) {
    let alert = this.forgotCtrl.create({
      title: 'Login',
      subTitle: text,
      buttons: ['Fermer']
    });
    alert.present();
  }

  forgotPass() {
    let forgot = this.forgotCtrl.create({
      title: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
            let toast = this.toastCtrl.create({
              message: 'Email was sended successfully',
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'OK',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

}
