import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeroulementRepasPage } from './deroulement-repas';

@NgModule({
  declarations: [
    DeroulementRepasPage,
  ],
  imports: [
    IonicPageModule.forChild(DeroulementRepasPage),
  ],
})
export class DeroulementRepasPageModule {}
