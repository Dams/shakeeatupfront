import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { MainProvider } from '../../providers/provider';
import { SeanceTrainPage } from '../seance-train/seance-train';
import { SeancePage } from '../seance/seance';

/**
 * Generated class for the DeroulementRepasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-deroulement-repas',
  templateUrl: 'deroulement-repas.html',
})
export class DeroulementRepasPage {

  public repUn: Array<Object>;
  public repDeux: Array<Object>;
  public repTrois: Array<Object>;
  public seanceId: string;
  public reps: Array<Object>;

  constructor(public nav: NavController, public navParams: NavParams, public myProvider: MainProvider, private alertCtrl: AlertController) {
    this.seanceId = this.navParams.get('id');
    this.reps = new Array<Object>();
    this.repUn = new Array<Object>();
    this.repDeux = new Array<Object>();
    this.repTrois = new Array<Object>();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeroulementRepasPage');
  }

  async ngOnInit() {
    let allReps = {};
    allReps = await this.myProvider.get("repas/seance/" + this.seanceId);
    if ("resultat" in allReps) {
      this.reps = allReps["resultat"];
    }
    
    this.repUn = allReps["resultat"][0];
    this.repDeux = allReps["resultat"][1];
    this.repTrois = allReps["resultat"][2];
  }
  async validateSeance() {
    let seanceEnCours = await this.myProvider.get("seances/" + this.seanceId);
    let myObj = {};
    console.log(seanceEnCours);
    if ("resultat" in seanceEnCours) {
      console.log("2");
      myObj = {
        "createdAt": seanceEnCours["resultat"]["createdAt"],
        "updatedAt": seanceEnCours["resultat"]["updatedAt"],
        "id": seanceEnCours["resultat"]["id"],
        "etat": 2,
        "programme": seanceEnCours["resultat"]["programme"],
        "user": seanceEnCours["resultat"]["user"],
        "nom": seanceEnCours["resultat"]["nom"],
        "exp": seanceEnCours["resultat"]["exp"]
      }
      let updateSeance = await this.myProvider.post("seances/update", myObj);
      if ("resultat" in updateSeance) {
        console.log("3");
        this.nav.push(SeancePage, { id : this.navParams.get('idProgramme') });
      } 
    }
    
  }

}
