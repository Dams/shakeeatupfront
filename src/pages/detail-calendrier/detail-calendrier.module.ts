import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailCalendrierPage } from './detail-calendrier';

@NgModule({
  declarations: [
    DetailCalendrierPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailCalendrierPage),
  ],
})
export class DetailCalendrierPageModule {}
