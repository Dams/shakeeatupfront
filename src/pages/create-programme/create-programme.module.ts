import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateProgrammePage } from './create-programme';

@NgModule({
  declarations: [
    CreateProgrammePage,
  ],
  imports: [
    IonicPageModule.forChild(CreateProgrammePage),
  ],
})
export class CreateProgrammePageModule {}
