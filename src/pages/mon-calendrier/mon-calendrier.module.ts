import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MonCalendrierPage } from './mon-calendrier';

@NgModule({
  declarations: [
    MonCalendrierPage,
  ],
  imports: [
    IonicPageModule.forChild(MonCalendrierPage),
  ],
})
export class MonCalendrierPageModule {}
