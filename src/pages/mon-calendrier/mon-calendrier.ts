import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CalendarComponentOptions, DayConfig } from 'ion2-calendar';
import { MainProvider } from '../../providers/provider';
import { Storage } from "@ionic/storage";

/**
 * Generated class for the MonCalendrierPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mon-calendrier',
  templateUrl: 'mon-calendrier.html',
})
export class MonCalendrierPage {
  dateMulti: string[];
  type: 'string'; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
  daysConf: DayConfig[] = [];

  optionsMulti: CalendarComponentOptions = {
    //from : new Date(),
    //to : new Date(),
    pickMode: 'multi',
    //defaultSubtitle: 'Programmes du jour',
    //showMonthPicker: false,
    //showToggleButtons: false,
    color: 'primary',
    weekdays: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
    weekStart: 1,
    daysConfig: this.daysConf 
  };

  public programmesTrain: Array<object>;
  public plansDiet: Array<object>;
  public userData: {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public myProvider: MainProvider, public storage: Storage) { 
  }

  async ngOnInit() {
    this.initData();
  }
  initData() {
    let ctx = this;
    this.storage.get('dataUser').then(async (val) => {
        ctx.userData = val;
        let allProgrammesTrain: any;
        let allPlanDiet: any;
        allProgrammesTrain = await this.myProvider.get("programmestraining/client/" + val["id"]);
        allPlanDiet = await this.myProvider.get("programmes/client/" + val["id"]);
        if ("resultat" in allProgrammesTrain) {
            this.programmesTrain = allProgrammesTrain["resultat"];
        }
        if ("resultat" in allPlanDiet) {
          this.plansDiet = allPlanDiet["resultat"];
        }
        
        let keys = Object.keys(this.plansDiet);
        let len = keys.length;
        console.log(len);
        for (let i = 15; i < len; i++) {
          this.daysConf.push({
            date: new Date(2019, 5, i + 1),
            subTitle: `$${i + 1}`
          })
        }
    }
    );
}
  onChange($event) {
    console.log($event);
  }
  
  detailJour($event) {
    console.log("Détail séance");
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad MonCalendrierPage');
  }

}
