import { SeanceTrainDetailPage } from './../seance-train-detail/seance-train-detail';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { MainProvider } from '../../providers/provider';
import { Storage } from '@ionic/storage';
import { CreateSeanceTrainPage } from '../create-seance-train/create-seance-train';

/**
 * Generated class for the SeanceTrainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-seance-train',
  templateUrl: 'seance-train.html',
})
export class SeanceTrainPage {

  // list of programmes
  public mesSeancesTrain: any;


  // view trip detail
  viewDetail(id) {

  }

  userData: {};
  seancesTrain: Array<Object>
  role:number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public myProvider: MainProvider, public storage: Storage, private alertCtrl: AlertController) {
      this.seancesTrain = new Array<Object>();
  }

  ionViewDidLoad() {
      console.log('ionViewDidLoad SeancesTrainPage');
  }

  ngOnInit() {
      this.initData()
  }

  initData() {
      let ctx = this;
      this.storage.get('dataUser').then(async (val) => {
          ctx.userData = val;
          ctx.role = val["role"]
          let allSeancesTrain: any;
          if (val["role"] == 1){
              console.log("seancestraining/programmes/" + this.navParams.get('id'));
              allSeancesTrain = await this.myProvider.get("seancestraining/programmes/" + this.navParams.get('id'));
          }
          if (val["role"] == 10){
              allSeancesTrain = await this.myProvider.get("seancestraining/coach/" + val["id"]);
          }
          if ("resultat" in allSeancesTrain) {
              this.seancesTrain = allSeancesTrain["resultat"];

          }
      }
      );
  }

  createSeanceTrain() {

      new Promise((resolve, reject) => {
          this.navCtrl.push(CreateSeanceTrainPage, { resolve: resolve });
      }).then(data => {
          this.initData()
      });
  }
  updateSeanceTrain(data) {
      new Promise((resolve, reject) => {
          this.navCtrl.push(SeanceTrainDetailPage, { id: data, idProgramme: this.navParams.get('id'), resolve: resolve });
      }).then(data => {
          this.initData()
      });


  }
}
