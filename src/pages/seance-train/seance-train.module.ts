import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeanceTrainPage } from './seance-train';

@NgModule({
  declarations: [
    SeanceTrainPage,
  ],
  imports: [
    IonicPageModule.forChild(SeanceTrainPage),
  ],
})
export class SeanceTrainPageModule {}
