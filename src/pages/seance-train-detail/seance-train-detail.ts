import { DeroulementTrainingPage } from './../deroulement-training/deroulement-training';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MainProvider } from '../../providers/provider';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the SeanceTrainDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-seance-train-detail',
  templateUrl: 'seance-train-detail.html',
})
export class SeanceTrainDetailPage {

 
  // seance info
  public seance: any;
  public nom: "";
  public userData: {};
  public exos: Array<Object>
  public role: number;
  signupform: FormGroup;
  public exoUn: string;
  public exoDeux: string;
  public exoTrois: string;

  constructor(public nav: NavController, public navParams: NavParams, public storage: Storage, public myProvider: MainProvider, private alertCtrl: AlertController) {
    // set sample data
    //this.seance = tripService.getItem(this.navParams.get('id'));
    this.exos = new Array<Object>();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SeanceDetailPage');
  }

  async ngOnInit() {
    this.storage.get('dataUser').then(async (val) => {
      this.userData = val;
      this.role = val["role"];
      console.log(val["role"])
      let maSeance = await this.myProvider.get("seancestraining/" + this.navParams.get('id'));
      if ("resultat" in maSeance) {
        this.seance = maSeance["resultat"];
        this.nom = maSeance["resultat"]["nom"]
          let allExos = {}; 
          if(val["role"] == 10){
              allExos = await this.myProvider.get("exercice/coach/" + val['id']);
          }
        else{
              allExos = await this.myProvider.get("exercice/seance/" +  this.navParams.get('id'));
          }

        if ("resultat" in allExos) {
            console.log(allExos["resultat"].length);
            if(val["role"] == 10){
                for (let index = 0; index < allExos["resultat"].length; index++) {
                    if (allExos["resultat"][index]["seances"].includes(maSeance["resultat"]["id"])) {
                        let i = allExos["resultat"][index]["seances"].indexOf(maSeance["resultat"]["id"])
                        allExos["resultat"][index]["seances"].splice(i, 1);
                    }
                }
          }
          this.exos = allExos["resultat"];
            for (let i = 0; i<allExos["resultat"].length;i++){
                if(i == 0 && i <= allExos["resultat"].length)
                    this.exoUn = allExos["resultat"][i]["nom"];

                if(i == 1 && i <= allExos["resultat"].length)
                    this.exoDeux = allExos["resultat"][i]["nom"];

                if(i == 2 && i <= allExos["resultat"].length)
                    this.exoTrois = allExos["resultat"][i]["nom"];
            }
        }
      }
    });

      this.signupform = new FormGroup({
          exoUn: new FormControl('', Validators.required),
          exoDeux: new FormControl('', Validators.required),
          exoTrois: new FormControl('', Validators.required)
      });

  }

  async launchSeance(){
    console.log(this.navParams.get('id'));
    this.nav.push(DeroulementTrainingPage, {id : this.navParams.get('id'), idProgramme : this.navParams.get('idProgramme')});
  }

  async updateSeance(data) {
    let ctx = this;
    let myObj = {
      "createdAt": this.seance["createdAt"],
      "updatedAt": this.seance["updatedAt"],
      "id": this.seance["id"],
      "etat": this.seance["etat"],
      "programme": this.seance["programme"],
      "user": this.seance["user"],
      "nom": data.value.nom
    }

      this.storage.get('dataUser').then(async (val) => {
              ctx.userData = val;
              ctx.role = val["role"];
              let allSeances: any;
              if (val["role"] == 1){
                  let updateSeance = await this.myProvider.post("seances/update", myObj);
                  if ("resultat" in updateSeance) {
                    myObj["etat"] = 3;
                    ctx.presentAlert("La séance a été validée");
                    ctx.nav.pop().then(() => ctx.navParams.get('resolve')('some data'));
                  }
              }
              if (val["role"] == 10){
                  let updateSeance = await this.myProvider.post("seances/update", myObj);
                  if ("resultat" in updateSeance) {
                      ctx.exos[data.value.repasMa]["seances"].push(ctx.seance["id"]);
                      ctx.exos[data.value.repasMi]["seances"].push(ctx.seance["id"]);
                      ctx.exos[data.value.repasDi]["seances"].push(ctx.seance["id"]);
                      let uprepas1 = await ctx.myProvider.post("repas/update", ctx.exos[data.value.repasMa]);
                      let uprepas2 = await ctx.myProvider.post("repas/update", ctx.exos[data.value.repasMi]);
                      let uprepas3 = await ctx.myProvider.post("repas/update", ctx.exos[data.value.repasDi]);
                      ctx.presentAlert("La création a réussi");
                      ctx.nav.pop().then(() => ctx.navParams.get('resolve')('some data'))
                  }
                  else {
                      this.presentAlert("Une erreur est survenue");
                  }
              }
          }
      );


  }

  presentAlert(text) {
    let alert = this.alertCtrl.create({
      title: "Modification d'une seance",
      subTitle: text,
      buttons: ['Fermer']
    });
    alert.present();
  }


}
