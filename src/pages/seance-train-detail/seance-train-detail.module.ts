import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeanceTrainDetailPage } from './seance-train-detail';

@NgModule({
  declarations: [
    SeanceTrainDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(SeanceTrainDetailPage),
  ],
})
export class SeanceTrainDetailPageModule {}
