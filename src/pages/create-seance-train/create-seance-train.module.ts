import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateSeanceTrainPage } from './create-seance-train';

@NgModule({
  declarations: [
    CreateSeanceTrainPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateSeanceTrainPage),
  ],
})
export class CreateSeanceTrainPageModule {}
