import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MainProvider } from "../../providers/provider";
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ModifSceancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modif-sceance',
  templateUrl: 'modif-sceance.html',
})
export class ModifSceancePage {
  signupform: FormGroup;

  repas: Array<object>;

  repasMa: number;
  repasMi: number;
  repasDi: number;
  exp: number;
  userData: {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public myProvider: MainProvider, public storage: Storage, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateSceancePage');

  }

  async ngOnInit() {
    this.storage.get('dataUser').then(async (val) => {
      this.userData = val;
      let allRepas = await this.myProvider.get("repas/coach/" + val['id']);

      if ("resultat" in allRepas) {

        this.repas = allRepas["resultat"];

      }
    });
    this.signupform = new FormGroup({
      nom: new FormControl('', Validators.required),
      repasMa: new FormControl('', Validators.required),
      repasMi: new FormControl('', Validators.required),
      repasDi: new FormControl('', Validators.required),
      exp: new FormControl('', Validators.required)
    });


  }

  async updateSeance(signupform) {
    let update = await this.myProvider.post("seances/update", {
      "etat": 1,
      'programme': [],
      'user': this.userData["id"],
      "nom": signupform.value.nom,
      "exp": signupform.value.exp
    })

    if ("id" in update['resultat']) {
      this.repas[signupform.value.repasMa]["seances"].push(update['resultat']["id"]);
      this.repas[signupform.value.repasMi]["seances"].push(update['resultat']["id"]);
      this.repas[signupform.value.repasDi]["seances"].push(update['resultat']["id"]);
      this.repas[signupform.value.repasMa]["periode"].push(1);
      this.repas[signupform.value.repasMi]["periode"].push(2);
      this.repas[signupform.value.repasDi]["periode"].push(3);
      let uprepas1 = await this.myProvider.post("repas/update", this.repas[signupform.value.repasMa]);
      let uprepas2 = await this.myProvider.post("repas/update", this.repas[signupform.value.repasMi]);
      let uprepas3 = await this.myProvider.post("repas/update", this.repas[signupform.value.repasDi]);
      this.presentAlert("La mise à jour a réussi");
      this.navCtrl.pop().then(() => this.navParams.get('resolve')('some data'))
    }
    else {
      this.presentAlert("Une erreur est survenue");
    }
  }
  presentAlert(text) {
    let alert = this.alertCtrl.create({
      title: "Modification d'une seance",
      subTitle: text,
      buttons: ['Fermer']
    });
    alert.present();
  }
}
