import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModifSceancePage } from './modif-sceance';

@NgModule({
  declarations: [
    ModifSceancePage,
  ],
  imports: [
    IonicPageModule.forChild(ModifSceancePage),
  ],
})
export class ModifSceancePageModule {}
