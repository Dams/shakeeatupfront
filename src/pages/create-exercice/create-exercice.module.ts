import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateExercicePage } from './create-exercice';

@NgModule({
  declarations: [
    CreateExercicePage,
  ],
  imports: [
    IonicPageModule.forChild(CreateExercicePage),
  ],
})
export class CreateExercicePageModule {}
