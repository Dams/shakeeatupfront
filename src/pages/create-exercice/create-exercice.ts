import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { MainProvider } from '../../providers/provider';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the CreateExercicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-exercice',
  templateUrl: 'create-exercice.html',
})
export class CreateExercicePage {
  signupform: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public myProvider: MainProvider, public storage: Storage, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateExercicePage');
  }

  ngOnInit() {
    this.signupform = new FormGroup({
      title: new FormControl('', Validators.required),
      desc: new FormControl('', Validators.required),
      grpMuscle: new FormControl('', Validators.required),
      serie: new FormControl('', Validators.required),
      rep: new FormControl('', Validators.required),
      tpsrecup: new FormControl('', Validators.required),
      exp: new FormControl('', Validators.required)
    });
  }

  async createExo(data) {

    this.storage.get('dataUser').then(async (val) => {
      let myObj = {
        "nom": data.value.title,
        "infos": data.value.desc,
        "seances": [],
        "groupemuscle": data.value.grpMuscle,
        "serie":data.value.serie,
        "rep":data.value.rep,
        "tpsrecup":data.value.tpsrecup,
        "user": val.id,
        "exp": data.value.exp
      };
      let createExercice = await this.myProvider.post("exercice/create", myObj);
      if ("id" in createExercice["resultat"]) {
        this.presentAlert("La création a réussi");
        this.navCtrl.pop().then(() => this.navParams.get('resolve')('some data'))
      }

      else
        this.presentAlert("Une erreur est survenue");
    }
    );

  }

  presentAlert(text) {
    let alert = this.alertCtrl.create({
      title: "Création de l'exercice",
      subTitle: text,
      buttons: ['Fermer']
    });
    alert.present();
  }
}