import { ModifExercicePage } from './../modif-exercice/modif-exercice';
import { CreateExercicePage } from './../create-exercice/create-exercice';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { MainProvider } from '../../providers/provider';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ExercicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-exercice',
  templateUrl: 'exercice.html',
})
export class ExercicePage {
  userData: {};
  exos: Array<Object>
  constructor(public navCtrl: NavController, public navParams: NavParams, public myProvider: MainProvider, public storage: Storage, private alertCtrl: AlertController) {
    this.exos = new Array<Object>();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExercicePage');
    this.initData();
  }
  ngOnInit() {
    this.initData()
  }

  initData() {
    let ctx = this;
    this.storage.get('dataUser').then(async (val) => {
      ctx.userData = val;
      console.log("http://localhost:1337/exercice/coach/" + ctx.userData["id"]);
      let allExos = await this.myProvider.get("exercice/coach/" + ctx.userData["id"]);
      /*let allRepas = await this.myProvider.get("repas/all");*/
      if ("resultat" in allExos) {
        this.exos = allExos["resultat"];
      }
    }
    );
  }

  createExo() {
    new Promise((resolve, reject) => {
      this.navCtrl.push(CreateExercicePage, { resolve: resolve });
    }).then(data => {
      this.initData();
    });

  }
  updateExo(data) {
    new Promise((resolve, reject) => {
      this.navCtrl.push(ModifExercicePage, { id: data, resolve: resolve });
    }).then(data => {
      this.initData();
    });


  }

}

