import {Component} from "@angular/core";
import {NavController} from "ionic-angular";
import {TripService} from "../../services/trip-service";
import {TripDetailPage} from "../trip-detail/trip-detail";
import {MainProvider} from "../../providers/provider";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'page-trips',
  templateUrl: 'trips.html'
})
export class TripsPage {
  // list of trips
  public programmesDiet: Array<object>;
  public programmesTrain: Array<object>;

  constructor(public nav: NavController, public myProvider: MainProvider, public storage: Storage) {
  }

    async ngOnInit() {
      console.log("resultat 0");
        let allProgrammes = await this.myProvider.get("programmes/all");
        if ("resultat" in allProgrammes) {
            console.log("resultat 1");
            this.programmesDiet = allProgrammes["resultat"];
            console.log(this.programmesDiet);
        }
        let allProgrammesTrain = await this.myProvider.get("programmestraining/all");
        if ("resultat" in allProgrammesTrain) {
          console.log("resultat 2");
            this.programmesTrain = allProgrammesTrain["resultat"];
        }  
        console.log("resultat 3");
        
    }

  // view trip detail
  viewDetail(id) {
    this.nav.push(TripDetailPage, {id});
  }
}
