import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MainProvider } from '../../providers/provider';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the ModifSeanceTrainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modif-seance-train',
  templateUrl: 'modif-seance-train.html',
})
export class ModifSeanceTrainPage {

  signupform: FormGroup;

  exos: Array<object>;

  exoUn: number;
  exoDeux: number;
  exoTrois: number;
  exp: number;
  userData: {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public myProvider: MainProvider, public storage: Storage, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateSceancePage');

  }

  async ngOnInit() {
    this.storage.get('dataUser').then(async (val) => {
      this.userData = val;
      let allExos = await this.myProvider.get("exercice/coach/" + val['id']);

      if ("resultat" in allExos) {

        this.exos = allExos["resultat"];

      }
    });
    this.signupform = new FormGroup({
      nom: new FormControl('', Validators.required),
      exoUn: new FormControl('', Validators.required),
      exoDeux: new FormControl('', Validators.required),
      exoTrois: new FormControl('', Validators.required)
    });


  }

  async updateSeanceTrain(signupform) {
    let update = await this.myProvider.post("seancestraining/create", {
      "etat": 1,
      'programme': [],
      'user': this.userData["id"],
      "nom": signupform.value.nom
    })

    if ("id" in update['resultat']) {
      this.exos[signupform.value.exoUn]["seances"].push(update['resultat']["id"]);
      this.exos[signupform.value.exoDeux]["seances"].push(update['resultat']["id"]);
      this.exos[signupform.value.exoTrois]["seances"].push(update['resultat']["id"]);
      /*this.exos[signupform.value.exoUn]["periode"].push(1);
      this.exos[signupform.value.exoDeux]["periode"].push(2);
      this.exos[signupform.value.exoTrois]["periode"].push(3);*/
      let uprepas1 = await this.myProvider.post("exercice/update", this.exos[signupform.value.exoUn]);
      let uprepas2 = await this.myProvider.post("exercice/update", this.exos[signupform.value.exoDeux]);
      let uprepas3 = await this.myProvider.post("exercice/update", this.exos[signupform.value.exoTrois]);
      this.presentAlert("La modification a été réussite");
      this.navCtrl.pop().then(() => this.navParams.get('resolve')('some data'))
    }
    else {
      this.presentAlert("Une erreur est survenue");
    }
  }
  presentAlert(text) {
    let alert = this.alertCtrl.create({
      title: "Modification d'une seance training",
      subTitle: text,
      buttons: ['Fermer']
    });
    alert.present();
  }

}
