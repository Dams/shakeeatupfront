import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModifSeanceTrainPage } from './modif-seance-train';

@NgModule({
  declarations: [
    ModifSeanceTrainPage,
  ],
  imports: [
    IonicPageModule.forChild(ModifSeanceTrainPage),
  ],
})
export class ModifSeanceTrainPageModule {}
