import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { CreateRepasPage } from "../create-repas/create-repas";
import { ModifRepasPage } from "../modif-repas/modif-repas";
import { Storage } from '@ionic/storage';
import { MainProvider } from "../../providers/provider";

/**
 * Generated class for the CreateRepasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-repas',
  templateUrl: 'repas.html',
})
export class RepasPage {
  userData: {};
  repas: Array<Object>

  constructor(public navCtrl: NavController, public navParams: NavParams, public myProvider: MainProvider, public storage: Storage, private alertCtrl: AlertController) {
    this.repas = new Array<Object>();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RepasPage');
    this.initData()
  }

  ngOnInit() {
    this.initData()
  }

  initData() {
    let ctx = this;
    console.log("0");
    this.storage.get('dataUser').then(async (val) => {
      ctx.userData = val;
      console.log("1");
      console.log("http://localhost:1337/repas/coach/" + ctx.userData["id"]);
      let allRepas = await this.myProvider.get("repas/coach/" + ctx.userData["id"]);
      /*let allRepas = await this.myProvider.get("repas/all");*/
      if ("resultat" in allRepas) {
        console.log("2");
        this.repas = allRepas["resultat"];
      }
      else {
        console.log("3");
      }
    }
    );
  }

  createRepas() {
    new Promise((resolve, reject) => {
      this.navCtrl.push(CreateRepasPage, { resolve: resolve });
    }).then(data => {
      this.initData()
    });

  }
  updateRepas(data) {
    new Promise((resolve, reject) => {
      this.navCtrl.push(ModifRepasPage, { id: data, resolve: resolve });
    }).then(data => {
      this.initData()
    });


  }

}
