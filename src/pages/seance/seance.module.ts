import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeancePage } from './seance';

@NgModule({
  declarations: [
    SeancePage,
  ],
  imports: [
    IonicPageModule.forChild(SeancePage),
  ],
})
export class SeancePageModule {}
