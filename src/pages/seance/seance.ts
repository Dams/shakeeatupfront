import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { SeanceDetailPage } from "../seance-detail/seance-detail";
import { CreateSceancePage } from "../create-sceance/create-sceance";
import { Storage } from '@ionic/storage';
import { MainProvider } from "../../providers/provider";

@Component({
    selector: 'page-seance',
    templateUrl: 'seance.html'
})
export class SeancePage {
    // list of programmes
    public mesSeances: any;


    // view trip detail
    viewDetail(id) {

    }

    userData: {};
    seances: Array<Object>
    role:number;

    constructor(public navCtrl: NavController, public navParams: NavParams, public myProvider: MainProvider, public storage: Storage, private alertCtrl: AlertController) {
        this.seances = new Array<Object>();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SeancesPage');
    }

    ngOnInit() {
        this.initData()
    }

    initData() {
        let ctx = this;
        this.storage.get('dataUser').then(async (val) => {
            ctx.userData = val;
            ctx.role = val["role"]
            let allSeances: any;
            if (val["role"] == 1){
                allSeances = await this.myProvider.get("seances/programmes/" + this.navParams.get('id'));
            }
            if (val["role"] == 10){
                allSeances = await this.myProvider.get("seances/coach/" + val["id"]);
            }
            if ("resultat" in allSeances) {
                this.seances = allSeances["resultat"];

            }
        }
        );
    }

    createSeance() {

        new Promise((resolve, reject) => {
            this.navCtrl.push(CreateSceancePage, { resolve: resolve });
        }).then(data => {
            this.initData()
        });
    }
    updateSeance(data) {
        new Promise((resolve, reject) => {
            this.navCtrl.push(SeanceDetailPage, { id: data, idProgramme: this.navParams.get('id'), resolve: resolve });
        }).then(data => {
            this.initData()
        });


    }
}
