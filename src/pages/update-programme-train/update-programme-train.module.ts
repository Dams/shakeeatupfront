import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateProgrammeTrainPage } from './update-programme-train';

@NgModule({
  declarations: [
    UpdateProgrammeTrainPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateProgrammeTrainPage),
  ],
})
export class UpdateProgrammeTrainPageModule {}
