import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterCoachPage } from './register-coach';

@NgModule({
  declarations: [
    RegisterCoachPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterCoachPage),
  ],
})
export class RegisterCoachPageModule {}
