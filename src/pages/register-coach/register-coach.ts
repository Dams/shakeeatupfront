import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { HomePage } from "../home/home";
import { RegisterPage } from "../register/register";
import { LoginPage } from "../login/login";
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { MainProvider } from "../../providers/provider";

/**
 * Generated class for the RegisterCoachPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-coach',
  templateUrl: 'register-coach.html',
})
export class RegisterCoachPage {

  signupform: FormGroup;
  userData = {
    "email": "",
    "password": "",
    "prenom": "",
    "nom": "",
    "adresse": "",
    "ville": "",
    "cp": "",
    "naissance": "",
    "sex": ""
  };

  constructor(public nav: NavController, public myProvider: MainProvider, private storage: Storage, private alertCtrl: AlertController) {
  }

  ngOnInit() {
    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.signupform = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern(EMAILPATTERN)]),
      nom: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2), Validators.maxLength(50)]),
      prenom: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2), Validators.maxLength(50)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(12)]),
      naissance: new FormControl('', Validators.required),
    });
  }

  async register(data) {
    let myObj = {
      "email": data.value.email,
      "nom": data.value.nom,
      "prenom": data.value.prenom,
      "password": data.value.password,
      "naissance": data.value.naissance,
      "role": "10",
    }

    let register = await this.myProvider.post("users/create", myObj);

    if ("resultat" in register) {
      let login = await this.myProvider.post("auth/login", { email: data.value.email, password: data.value.password });

      // if ("token" in login) {
        this.storage.set("token", login["token"])
        this.storage.set("dataUser", login["resultat"])
        this.nav.setRoot(LoginPage);
        this.presentAlert("Votre compte à bien été créé !")
      // }
      // else {
      //   this.presentAlert("Une erreur est survenue")
      //   this.storage.set("dataUser", login["resultat"])
      //   this.nav.setRoot(HomePage);
      // }

    }
  }
  // go to login page
  login() {
    this.nav.setRoot(LoginPage);
  };
  presentAlert(text) {
    let alert = this.alertCtrl.create({
      title: 'Inscription',
      subTitle: text,
      buttons: ['Fermer']
    });
    alert.present();
  }

}
