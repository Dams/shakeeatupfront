import { SeanceTrainPage } from './../seance-train/seance-train';
import { Component } from "@angular/core";
import { NavController, IonicPage } from "ionic-angular";
import { MainProvider } from "../../providers/provider";
import { Storage } from "@ionic/storage";
import { SeancePage } from "../seance/seance";
import { CreateProgrammeTrainPage } from "../create-programme-train/create-programme-train";

/**
 * Generated class for the ProgrammeTrainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-programme-train',
  templateUrl: 'programme-train.html',
})
export class ProgrammeTrainPage {
  // list of trips
  public programmes: Array<object>;
  public userData: {}
  public role: number;

  constructor(public nav: NavController, public myProvider: MainProvider, public storage: Storage) {
  }

  async ngOnInit() {
      this.initData();
  }

  // view trip detail
  viewDetail(id) {
      this.nav.push(SeanceTrainPage, { id });
  }

  initData() {
      let ctx = this;
      this.storage.get('dataUser').then(async (val) => {
          ctx.userData = val;
          this.role = val["role"];
          let allProgrammes: any;
          if (val["role"] == 1) {
              allProgrammes = await this.myProvider.get("programmestraining/client/" + val["id"]);
          }
          if (val["role"] == 10) {
              allProgrammes = await this.myProvider.get("programmestraining/coach/" + val["id"]);
          }
          if ("resultat" in allProgrammes) {
              this.programmes = allProgrammes["resultat"];
          }
      }
      );
  }

  createProgramme() {
      new Promise((resolve, reject) => {
          this.nav.push(CreateProgrammeTrainPage, { resolve: resolve });
      }).then(data => {
          this.initData()
      });
  }
}
