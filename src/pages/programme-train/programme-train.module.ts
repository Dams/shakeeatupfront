import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProgrammeTrainPage } from './programme-train';

@NgModule({
  declarations: [
    ProgrammeTrainPage,
  ],
  imports: [
    IonicPageModule.forChild(ProgrammeTrainPage),
  ],
})
export class ProgrammeTrainPageModule {}
