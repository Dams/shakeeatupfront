import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateProgrammeTrainPage } from './create-programme-train';

@NgModule({
  declarations: [
    CreateProgrammeTrainPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateProgrammeTrainPage),
  ],
})
export class CreateProgrammeTrainPageModule {}
