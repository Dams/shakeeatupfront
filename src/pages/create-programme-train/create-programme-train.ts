import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MainProvider } from "../../providers/provider";
import { Storage } from '@ionic/storage';

/**
 * Generated class for the CreateProgrammeTrainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-programme-train',
  templateUrl: 'create-programme-train.html',
})
export class CreateProgrammeTrainPage {

  signupform: FormGroup;
  seances: Array<object>;
  userData: {}
  seancesIndex: Array<number>

  constructor(public navCtrl: NavController, public navParams: NavParams, public myProvider: MainProvider, public storage: Storage, private alertCtrl: AlertController) {
    this.seancesIndex = new Array<number>();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateProgrammeTrainPage');
  }

  async ngOnInit() {
    this.storage.get('dataUser').then(async (val) => {
      this.userData = val;
      let allSeances = await this.myProvider.get("seancestraining/coach/" + val['id']);

      if ("resultat" in allSeances) {

        this.seances = allSeances["resultat"];

      }
    });
    this.signupform = new FormGroup({
      nom: new FormControl('', Validators.required),
      duree: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
      seances: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
    });

  }

  async createProgramme(data) {
    let ctx = this;
    let myObj = {
      "nom": data.value.nom,
      "duree": data.value.duree,
      "description": data.value.description,
      "user": this.userData["id"],
      "type": data.value.type
    }
    let createdProgramme = await this.myProvider.post("programmestraining/create", myObj);
    if ("id" in createdProgramme["resultat"]) {
      this.seancesIndex.forEach(async element => {
        console.log(ctx.seances[element]);
        ctx.seances[element]["programme"].push(createdProgramme["resultat"]["id"])
        await ctx.myProvider.post("seancestraining/update", ctx.seances[element])
      });
      this.presentAlert("La création a été réussi")
      this.navCtrl.pop().then(() => this.navParams.get('resolve')('some data'))
    }
    else {
      this.presentAlert("Une erreur est survenue")
    }
  }

  clickSeance(data) {
    console.log(this.seancesIndex)
    this.seancesIndex.push(data);
  }
  
  presentAlert(text) {
    let alert = this.alertCtrl.create({
      title: "Création d'un programme",
      subTitle: text,
      buttons: ['Fermer']
    });
    alert.present();
  }

}
