import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeanceDetailPage } from './seance-detail';

@NgModule({
  declarations: [
    SeanceDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(SeanceDetailPage),
  ],
})
export class SeanceDetailPageModule {}
