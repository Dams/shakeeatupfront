import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MainProvider } from "../../providers/provider";
import { Storage } from '@ionic/storage';
import { DeroulementRepasPage } from '../deroulement-repas/deroulement-repas';

/**
 * Generated class for the SeanceDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-seance-detail',
  templateUrl: 'seance-detail.html',
})
export class SeanceDetailPage {

  // seance info
  public seance: any;
  public nom: "";
  public userData: {};
  public repas: Array<Object>
  public role: number;
  signupform: FormGroup;
  public repasMidi: string;
  public repasMatin: string;
  public repasDiner: string;

  constructor(public nav: NavController, public navParams: NavParams, public storage: Storage, public myProvider: MainProvider, private alertCtrl: AlertController) {
    // set sample data
    //this.seance = tripService.getItem(this.navParams.get('id'));
    this.repas = new Array<Object>();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SeanceTrainDetailPage');
  }

  async ngOnInit() {
    this.storage.get('dataUser').then(async (val) => {
      this.userData = val;
      this.role = val["role"];
      console.log(val["role"])
      let maSeance = await this.myProvider.get("seances/" + this.navParams.get('id'));
      if ("resultat" in maSeance) {
        this.seance = maSeance["resultat"];
        this.nom = maSeance["resultat"]["nom"]
          let allRepas = {};
          if(val["role"] == 10){
              allRepas = await this.myProvider.get("repas/coach/" + val['id']);
          }
        else{
              allRepas = await this.myProvider.get("repas/seance/" +  this.navParams.get('id'));
          }

        if ("resultat" in allRepas) {

            if(val["role"] == 10){
                for (let index = 0; index < allRepas["resultat"].length; index++) {
                    if (allRepas["resultat"][index]["seances"].includes(maSeance["resultat"]["id"])) {
                        let i = allRepas["resultat"][index]["seances"].indexOf(maSeance["resultat"]["id"])
                        allRepas["resultat"][index]["seances"].splice(i, 1);
                    }
                }
          }
          this.repas = allRepas["resultat"];
          console.log(this.repas);
            for (let i = 0; i<allRepas["resultat"].length;i++){
                if(i == 0)
                    this.repasMatin = allRepas["resultat"][i]["nom"];

                if(i == 1)
                    this.repasMidi = allRepas["resultat"][i]["nom"];

                if(i == 2)
                    this.repasDiner = allRepas["resultat"][i]["nom"];
            }
        }
      }
    });

      this.signupform = new FormGroup({
          repadMa: new FormControl('', Validators.required),
          repasMi: new FormControl('', Validators.required),
          repasDi: new FormControl('', Validators.required)
      });

  }

  async launchSeance(){
    console.log(this.navParams.get('id'));
    this.nav.push(DeroulementRepasPage, {id : this.navParams.get('id'), idProgramme : this.navParams.get('idProgramme')});
  }

  async updateSeance(data) {
    let ctx = this;
    let myObj = {
      "createdAt": this.seance["createdAt"],
      "updatedAt": this.seance["updatedAt"],
      "id": this.seance["id"],
      "etat": this.seance["etat"],
      "programme": this.seance["programme"],
      "user": this.seance["user"],
      "nom": data.value.nom
    }

      this.storage.get('dataUser').then(async (val) => {
              ctx.userData = val;
              ctx.role = val["role"];
              let allSeances: any;
              if (val["role"] == 1){
                  let updateSeance = await this.myProvider.post("seances/update", myObj);
                  if ("resultat" in updateSeance) {
                    myObj["etat"] = 3;
                    ctx.presentAlert("La séance a été validée");
                    ctx.nav.pop().then(() => ctx.navParams.get('resolve')('some data'));
                  }
              }
              if (val["role"] == 10){
                  let updateSeance = await this.myProvider.post("seances/update", myObj);
                  if ("resultat" in updateSeance) {
                      ctx.repas[data.value.repasMa]["seances"].push(ctx.seance["id"]);
                      ctx.repas[data.value.repasMi]["seances"].push(ctx.seance["id"]);
                      ctx.repas[data.value.repasDi]["seances"].push(ctx.seance["id"]);
                      let uprepas1 = await ctx.myProvider.post("repas/update", ctx.repas[data.value.repasMa]);
                      let uprepas2 = await ctx.myProvider.post("repas/update", ctx.repas[data.value.repasMi]);
                      let uprepas3 = await ctx.myProvider.post("repas/update", ctx.repas[data.value.repasDi]);
                      ctx.presentAlert("La création a réussi");
                      ctx.nav.pop().then(() => ctx.navParams.get('resolve')('some data'))
                  }
                  else {
                      this.presentAlert("Une erreur est survenue");
                  }
              }
          }
      );


  }

  presentAlert(text) {
    let alert = this.alertCtrl.create({
      title: "Modification d'une seance",
      subTitle: text,
      buttons: ['Fermer']
    });
    alert.present();
  }

}
