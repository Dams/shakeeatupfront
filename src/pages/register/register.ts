import { Component } from "@angular/core";
import { NavController, AlertController } from "ionic-angular";
import { LoginPage } from "../login/login";
import { HomePage } from "../home/home";
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { MainProvider } from "../../providers/provider";


@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {

  constructor(public nav: NavController, public myProvider: MainProvider, private storage: Storage, private alertCtrl: AlertController) {
  }
  registerForm: FormGroup;
  ngOnInit() {
    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.registerForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern(EMAILPATTERN)]),
      nom: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2), Validators.maxLength(50)]),
      prenom: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2), Validators.maxLength(50)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(12)]),
      naissance: new FormControl('', Validators.required),
      taille: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(3), Validators.min(20)]),
      poids: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(3), Validators.min(20)]),
      type_objectif: new FormControl(''),
    });

  }
  // register and go to home page

  async register(data) {
    let myObj = {
      "email": data.value.email,
      "nom": data.value.nom,
      "prenom": data.value.prenom,
      "password": data.value.password,
      "naissance": data.value.naissance,
      "taille": data.value.taille,
      "poids": data.value.poids,
      "role": "1",
      "objectifs": {
        "type": data.value.type_objectif
      }
    }

    let register = await this.myProvider.post("users/create", myObj);

    if ("resultat" in register) {
      let login = await this.myProvider.post("auth/login", { email: data.value.email, password: data.value.password });

      // if ("token" in login) {
        this.storage.set("token", login["token"])
        this.storage.set("dataUser", login["resultat"])
        this.nav.setRoot(LoginPage);
        this.presentAlert("Votre compte à bien été créé !")
      // }
      // else {
      //   this.presentAlert("Une erreur est survenue")
      // }

    }

  }

  // go to login page
  login() {
    this.nav.setRoot(LoginPage);
  }

  presentAlert(text) {
    let alert = this.alertCtrl.create({
      title: 'Inscription',
      subTitle: text,
      buttons: ['Fermer']
    });
    alert.present();
  }
}
