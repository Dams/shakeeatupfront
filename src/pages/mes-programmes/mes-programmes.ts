import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { TripService } from "../../services/trip-service";
import { TripDetailPage } from "../trip-detail/trip-detail";
import { CreateProgrammePage } from "../create-programme/create-programme";
import { MainProvider } from "../../providers/provider";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Storage } from "@ionic/storage";
import { SeancePage } from "../seance/seance";
import { SeanceTrainPage } from '../seance-train/seance-train';


@Component({
    selector: 'page-mes-programmes',
    templateUrl: 'mes-programmes.html'
})
export class MesProgrammesPage {
    // list of trips
    public programmes: Array<object>;
    public programmesTrain: Array<object>;
    public userData: {}
    public role: number;

    constructor(public nav: NavController, public myProvider: MainProvider, public storage: Storage) {
    }

    async ngOnInit() {
        this.initData();
    }

    // view Diet detail
    viewDetail(id) {
        this.nav.push(SeancePage, { id });
    }

    // view Train detail
    viewDetailTrain(id) {
        this.nav.push(SeanceTrainPage, { id });
    }

    initData() {
        let ctx = this;
        this.storage.get('dataUser').then(async (val) => {
            ctx.userData = val;
            this.role = val["role"];
            let allProgrammes: any;
            let allProgrammesTrain: any;
            if (val["role"] == 1) {
                allProgrammes = await this.myProvider.get("programmes/client/" + val["id"]);
                allProgrammesTrain = await this.myProvider.get("programmestraining/client/" + val["id"]);
            }
            if (val["role"] == 10) {
                allProgrammes = await this.myProvider.get("programmes/coach/" + val["id"]);
                allProgrammesTrain = await this.myProvider.get("programmestraining/client/" + val["id"]);
            }
            if ("resultat" in allProgrammes) {
                this.programmes = allProgrammes["resultat"];
            }
            if ("resultat" in allProgrammesTrain) {
                this.programmesTrain = allProgrammesTrain["resultat"];
            }
        }
        );
    }

    createProgramme() {

        new Promise((resolve, reject) => {
            this.nav.push(CreateProgrammePage, { resolve: resolve });
        }).then(data => {
            this.initData()
        });
    }
}
