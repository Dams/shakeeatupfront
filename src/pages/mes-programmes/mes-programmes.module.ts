import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MesProgrammesPage } from './mes-programmes';

@NgModule({
  declarations: [
    MesProgrammesPage,
  ],
  imports: [
    IonicPageModule.forChild(MesProgrammesPage),
  ],
})
export class MesProgrammesPageModule {}
