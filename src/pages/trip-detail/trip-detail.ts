import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { TripService } from "../../services/trip-service";
import { UpdateProgrammePage } from "../update-programme/update-programme";
import { CheckoutTripPage } from "../checkout-trip/checkout-trip";
import { MainProvider } from "../../providers/provider";
import { Storage } from "@ionic/storage";

@Component({
    selector: 'page-trip-detail',
    templateUrl: 'trip-detail.html'
})
export class TripDetailPage {
    // trip info
    public programme: Array<object>;
    public nom: "";
    public duree: number;
    public description: "";
    public type: number;
    public userData: object;
    public userRole: number;
    public isTrain: boolean;
    constructor(public nav: NavController, public myProvider: MainProvider, public navParams: NavParams, public storage: Storage) {
    }

    async ngOnInit() {
        let ctx = this;
        this.storage.get('dataUser').then((val) => {
            ctx.userData = val;
            ctx.userRole = val['role'];
        }
        );
        let leProgramme = await this.myProvider.get("programmes/" + this.navParams.get('id'));
        if ("resultat" in leProgramme) {
            this.programme = leProgramme["resultat"];
            this.nom = leProgramme["resultat"]["nom"];
            this.duree = leProgramme["resultat"]["duree"];
            this.description = leProgramme["resultat"]["description"];
            this.type = leProgramme["resultat"]["type"];
            this.isTrain = false;

        }
        else {
            let leProgramme = await this.myProvider.get("programmestraining/" + this.navParams.get('id'));
            if ("resultat" in leProgramme) {
                this.programme = leProgramme["resultat"];
                this.nom = leProgramme["resultat"]["nom"];
                this.duree = leProgramme["resultat"]["duree"];
                this.description = leProgramme["resultat"]["description"];
                this.type = leProgramme["resultat"]["type"]
                this.isTrain = true;
            }

        }
    }

    // go to checkout page
    addToMesSeances() {

        this.programme["clients"].push(this.userData["id"]);
        console.log(this.programme);
        if (this.isTrain == false) {
            let leUpdateProgramme = this.myProvider.post("programmes/update", this.programme);
        }
        if (this.isTrain == true) {
            let leUpdateProgramme = this.myProvider.post("programmestraining/update", this.programme);
        }
    }

    updateProgramme() {
        let ctx = this;
        this.nav.push(UpdateProgrammePage, { id: this.programme["id"] });
    }
}
