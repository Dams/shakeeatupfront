import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateSceancePage } from './create-sceance';

@NgModule({
  declarations: [
    CreateSceancePage,
  ],
  imports: [
    IonicPageModule.forChild(CreateSceancePage),
  ],
})
export class CreateSceancePageModule {}
