import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { HomePage } from "../home/home";
import { MainProvider } from "../../providers/provider";

/**
 * Generated class for the ProfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profil',
  templateUrl: 'profil.html',
})
export class ProfilPage {
  signupform: FormGroup;
  userData = {
  };

  constructor(public storage: Storage, public myProvider: MainProvider, private alertCtrl: AlertController) { //private navCtrl:NavController
    this.isShow = false;

  }

  ngOnInit() {
    this.isShow = false;

    let ctx = this;
    this.storage.get('dataUser').then((val) => {
        ctx.userData = val
      }
    );
    let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.signupform = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern(EMAILPATTERN)]),
      nom: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2), Validators.maxLength(50)]),
      prenom: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2), Validators.maxLength(50)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(12)]),
      naissance: new FormControl('', Validators.required),
      taille: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(3), Validators.min(20)]),
      poids: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(3), Validators.min(20)]),
      type_objectif: new FormControl(''),
      sex: new FormControl(''),
      adresse: new FormControl(''),
      ville: new FormControl(''),
      cp: new FormControl(''),

    });
  }

  isShow:false;

  switch(data){
    this.isShow = data;
  }

  // paid:false;
  //
  // statutPaid(data){
  //   this.paid = data;
  // }

  // async paiement(){
  //
  //   let abonnement = await this.myProvider.get("users/subscription/" + this.userData['id']);
  //
  //   if ("resultat" in abonnement) {
  //     this.storage.set("userData", abonnement['resultat']);
  //     this.userData = abonnement['resultat'];
  //   }
  //   console.log(this.userData);
  // }

  async update(data) {

    if (data.value.naissance == "")
      delete data.value.naissance

    if (data.value.poids == "")
      delete data.value.poids

    let myObj = {
      "createdAt": this.userData["createdAt"],
      "id": this.userData["id"],
      "email": data.value.email,
      "role": this.userData["role"],
      "prenom": data.value.prenom,
      "nom": data.value.nom,
      "sex": data.value.sex,
      "naissance": data.value.naissance,
      "adresse": data.value.adresse + "," + data.value.ville + "," + data.value.cp,
      "created": this.userData["created"],
      "poids_depart": data.value.poids,
      "objectifs": this.userData["objectifs"],
      "customerKey": this.userData["customerKey"]
    }

    let updateUser = await this.myProvider.post("users/update", myObj);

    this.presentAlert("Votre compte à bien mise à jour!")

  }

  doRefresh(event) {
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  presentAlert(text) {
    let alert = this.alertCtrl.create({
      title: 'Profil',
      subTitle: text,
      buttons: ['Fermer']
    });
    alert.present();
  }

}
