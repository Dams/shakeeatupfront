import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import Chart from 'chart.js';
import { Storage } from '@ionic/storage';
import { MainProvider } from "../../providers/provider";
import {DatePipe} from "@angular/common";

/**
 * Generated class for the GraphPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-graph',
  templateUrl: 'graph.html'
})
export class GraphPage {

  public userData: {

  }
  public suivis: {}
  public date: {}
  public poids: {}
  public commentaire: {}

  constructor(public navCtrl: NavController, public navParams: NavParams, public myProvider: MainProvider, public storage: Storage) {
  }

  ionViewDidLoad() {

    // var ctx = document.getElementById("myChart");
    //var ctx = "myChart";

    console.log('ionViewDidLoad GraphPage');
  }

  async ngOnInit() {
    let ctx = this;
    this.storage.get('dataUser').then(async (val) => {
      ctx.userData = val;

      let allSuivis: any;


      allSuivis = await this.myProvider.get("suivis/" + val["id"]);

      if ("resultat" in allSuivis) {
        this.suivis = allSuivis["resultat"];
        this.date = allSuivis["resultat"]["evolution"]["date"];
        this.poids = allSuivis["resultat"]["evolution"]["poids"];
        this.commentaire = allSuivis["resultat"]["evolution"]["commentaire"];

        this.iniGraph(allSuivis["resultat"]["evolution"]["date"], allSuivis["resultat"]["evolution"]["poids"])
      }
    }
    );
  }

  iniGraph(labels, data) {
    new Chart(document.getElementById("myChart"), {
      type: 'line',
      data: {
        labels: labels,
        datasets: [{
          data: data,
          label: "Vous",
          borderColor: "#3e95cd",
          fill: false
        }
        ]
      },
      options: {
        title: {
          display: true,
          text: 'Votre suivi'
        }
      }
    });
  }


}
