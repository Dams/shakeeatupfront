import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MainProvider } from "../../providers/provider";
import { Storage } from "@ionic/storage";

/**
 * Generated class for the ModifRepasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modif-repas',
  templateUrl: 'modif-repas.html',
})
export class ModifRepasPage {
  editform: FormGroup;

  rep: string;
  userData: {};
  infos: string;
  Nom: string;
  Seance: string;
  Proteine: string;
  Lipide: string;
  Glucide: string;
  Ingr: string;
  Prep: string;
  repasSelect: {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public myProvider: MainProvider, public storage: Storage, private alertCtrl: AlertController, public myViewController: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModifRepasPage');
  }

  async ngOnInit() {
    this.storage.get('dataUser').then(async (val) => {
      this.userData = val;
      let myRepas = await this.myProvider.get("repas/" + this.navParams.get('id'));

      if ("resultat" in myRepas) {

        this.repasSelect = myRepas['resultat'];

        this.infos = myRepas['resultat'].infos;
        this.Nom = myRepas['resultat'].nom;
        this.Proteine = myRepas['resultat']['infos']['proteines'];
        this.Lipide = myRepas['resultat']['infos']['lipides'];
        this.Glucide = myRepas['resultat']['infos']['glucides'];
        this.Ingr = myRepas['resultat'].ingredients;
        this.Prep = myRepas['resultat'].prepa;

      }
    });


    this.editform = new FormGroup({
      nom: new FormControl('', Validators.required),
      proteine: new FormControl('', Validators.required),
      lipide: new FormControl('', Validators.required),
      glucide: new FormControl('', Validators.required),
      ingredients: new FormControl('', Validators.required),
      prepa: new FormControl('', Validators.required)
    });

  }


  async modifRepas(editform) {
    let modif = await this.myProvider.post("repas/update",
      {
        "createdAt": this.repasSelect["createdAt"],
        "id": this.repasSelect["id"],
        "infos": {
          "glucides": editform.value.glucide,
          "lipides": editform.value.lipide,
          "proteines": editform.value.proteine,
        },
        "ingredients": editform.value.ingredients,
        "prepa":editform.value.prepa,
        "nom": editform.value.nom,
        "periode": this.repasSelect["periode"],
        "seances": this.repasSelect["seances"],
        "updatedAt": this.repasSelect["updatedAt"],
        "user": this.repasSelect["user"]
      });

    if ("resultat" in modif) {
      this.presentAlert("La modification a ete effectué");
      this.navCtrl.pop().then(() => this.navParams.get('resolve')('some data'))
    }
    else
      this.presentAlert("Une erreur est survenue")
  }
  presentAlert(text) {
    let alert = this.alertCtrl.create({
      title: 'Modification de repas',
      subTitle: text,
      buttons: ['Fermer']
    });
    alert.present();
  }

}
