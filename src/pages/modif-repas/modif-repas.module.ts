import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModifRepasPage } from './modif-repas';

@NgModule({
  declarations: [
    ModifRepasPage,
  ],
  imports: [
    IonicPageModule.forChild(ModifRepasPage),
  ],
})
export class ModifRepasPageModule {}
