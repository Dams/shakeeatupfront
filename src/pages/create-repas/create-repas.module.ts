import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateRepasPage } from './create-repas';

@NgModule({
  declarations: [
    CreateRepasPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateRepasPage),
  ],
})
export class CreateRepasPageModule {}
