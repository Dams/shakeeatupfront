import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Storage } from '@ionic/storage';
import { MainProvider } from "../../providers/provider";

/**
 * Generated class for the CreateRepasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-repas',
  templateUrl: 'create-repas.html',
})
export class CreateRepasPage {
  signupform: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public myProvider: MainProvider, public storage: Storage, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateRepasPage');
  }

  ngOnInit() {
    this.signupform = new FormGroup({
      title: new FormControl('', Validators.required),
      proteines: new FormControl('', Validators.required),
      lipides: new FormControl('', Validators.required),
      glucides: new FormControl('', Validators.required),
      ingredients: new FormControl('', Validators.required),
      prepa: new FormControl('', Validators.required),
    });
  }

  async createMeal(data) {

    this.storage.get('dataUser').then(async (val) => {
      let myObj = {
        "infos": {
          "proteines": data.value.proteines,
          "lipides": data.value.lipides,
          "glucides": data.value.glucides
        },
        "ingredients": data.value.ingredients,
        "prepa":data.value.prepa,
        "nom": data.value.title,
        "seances": [],
          "periode": [],
        "user": val.id
      };
      let createRepas = await this.myProvider.post("repas/create", myObj);
      if ("id" in createRepas["resultat"]) {
        this.presentAlert("La création a réussi");
        this.navCtrl.pop().then(() => this.navParams.get('resolve')('some data'))
      }

      else
        this.presentAlert("Une erreur est survenue");
    }
    );

  }

  presentAlert(text) {
    let alert = this.alertCtrl.create({
      title: 'Création de repas',
      subTitle: text,
      buttons: ['Fermer']
    });
    alert.present();
  }
}
