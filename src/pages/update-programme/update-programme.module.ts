import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateProgrammePage } from './update-programme';

@NgModule({
  declarations: [
    UpdateProgrammePage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateProgrammePage),
  ],
})
export class CreateProgrammePageModule {}
