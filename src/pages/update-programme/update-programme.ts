import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MainProvider } from "../../providers/provider";
import { Storage } from '@ionic/storage';

/**
 * Generated class for the CreateProgrammePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update-programme',
  templateUrl: 'update-programme.html',
})
export class UpdateProgrammePage {

  signupform: FormGroup;
  seances: Array<object>;
  userData: {}
  seancesIndex: Array<number>
  programmeData: Object

  constructor(public navCtrl: NavController, public navParams: NavParams, public myProvider: MainProvider, public storage: Storage, private alertCtrl: AlertController) {
    this.seancesIndex = new Array<number>();
    this.programmeData = new Object();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateProgrammePage');
  }

  async ngOnInit() {
    let ctx = this;
    this.storage.get('dataUser').then(async (val) => {
      this.userData = val;
      let leProgramme = await this.myProvider.get("programmes/" + ctx.navParams.get('id'));
      if ("resultat" in leProgramme) {
        ctx.programmeData = leProgramme["resultat"];
        let allSeances = await this.myProvider.get("seances/coach/" + val['id']);
        if ("resultat" in allSeances) {

          for (let index = 0; index < allSeances["resultat"].length; index++) {
            if (allSeances["resultat"][index]["programme"].includes(ctx.programmeData["id"])) {
              allSeances["resultat"][index]["isFlag"] = true
            }
            else {
              allSeances["resultat"][index]["isFlag"] = false
            }
          }
          this.seances = allSeances["resultat"];
        }
      }

    });
    this.signupform = new FormGroup({
      nom: new FormControl('', Validators.required),
      duree: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
      seances: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
    });

  }

  async updateProgramme(data) {
    let ctx = this;
    let updatedProgramme = await this.myProvider.post("programmes/update", data.value);
    if ("resultat" in updatedProgramme) {
      this.seancesIndex.forEach(async element => {
        delete ctx.seances[element]["isFlag"];
        if (ctx.seances[element]["programme"].includes(ctx.programmeData["id"])) {
          let index = ctx.seances[element]["programme"].indexOf(ctx.programmeData["id"])
          ctx.seances[element]["programme"].splice(index, 1)
        }
        else {
          ctx.seances[element]["programme"].push(ctx.programmeData["id"])
        }

        await ctx.myProvider.post("seances/update", ctx.seances[element])
      });
      this.presentAlert("La modification a été réussi")
    }
    else {
      this.presentAlert("Une erreur est survenue")
    }
  }

  clickSeance(data) {
    this.seancesIndex.push(data);
  }
  presentAlert(text) {
    let alert = this.alertCtrl.create({
      title: "Création d'un programme",
      subTitle: text,
      buttons: ['Fermer']
    });
    alert.present();
  }

}
