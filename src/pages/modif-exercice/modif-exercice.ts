import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { MainProvider } from '../../providers/provider';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ModifExercicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modif-exercice',
  templateUrl: 'modif-exercice.html',
})
export class ModifExercicePage {
  signupform: FormGroup;
  dataUser = {
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public myProvider: MainProvider, public storage: Storage, private alertCtrl: AlertController) {
    this.isShow = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModifExercicePage');
  }

  ngOnInit() {

    let ctx = this;
    this.storage.get('dataUser').then((val) => {
      ctx.dataUser = val
    });
    this.signupform = new FormGroup({
      title: new FormControl('', Validators.required),
      desc: new FormControl('', Validators.required),
      grpMuscle: new FormControl('', Validators.required),
      exp: new FormControl('', Validators.required)
    });
  }

  async updateExo(data) {

    this.storage.get('dataUser').then(async (val) => {
        let myObj = {
          "nom": data.value.title,
          "infos": data.value.desc,
          "seances": [],
          "groupemuscle": data.value.grpMuscle,
          "user": val.id,
          "exp": data.value.exp
        };
        let updateExercice = await this.myProvider.post("exercice/update", myObj);

        if ("id" in updateExercice["resultat"]) {
          this.presentAlert("La modification est réussite");
          this.navCtrl.pop().then(() => this.navParams.get('resolve')('some data'))
        }

        else
          this.presentAlert("Une erreur est survenue");
      }
    );

  }

  isShow:false;

  switch(data){
    this.isShow = data;
  }

  presentAlert(text) {
    let alert = this.alertCtrl.create({
      title: "Modification de l'exercice",
      subTitle: text,
      buttons: ['Fermer']
    });
    alert.present();
  }

}
