import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModifExercicePage } from './modif-exercice';

@NgModule({
  declarations: [
    ModifExercicePage,
  ],
  imports: [
    IonicPageModule.forChild(ModifExercicePage),
  ],
})
export class ModifExercicePageModule {}
