import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeroulementTrainingPage } from './deroulement-training';

@NgModule({
  declarations: [
    DeroulementTrainingPage,
  ],
  imports: [
    IonicPageModule.forChild(DeroulementTrainingPage),
  ],
})
export class DeroulementTrainingPageModule {}
