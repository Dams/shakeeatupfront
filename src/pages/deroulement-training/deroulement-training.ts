import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { MainProvider } from '../../providers/provider';
import { SeanceTrainPage } from '../seance-train/seance-train';
/**
 * Generated class for the DeroulementTrainingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-deroulement-training',
  templateUrl: 'deroulement-training.html',
})
export class DeroulementTrainingPage {

  public exoUn: Array<Object>;
  public exoDeux: Array<Object>;
  public exoTrois: Array<Object>;
  public seanceId: string;
  public exos: Array<Object>;

  constructor(public nav: NavController, public navParams: NavParams, public myProvider: MainProvider, private alertCtrl: AlertController) {
    this.seanceId = this.navParams.get('id');
    this.exos = new Array<Object>();
    this.exoUn = new Array<Object>();
    this.exoDeux = new Array<Object>();
    this.exoTrois = new Array<Object>();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeroulementTrainingPage');
    
  }
  async ngOnInit() {
    let allExos = {};
    allExos = await this.myProvider.get("exercice/seance/" + this.seanceId);
    if ("resultat" in allExos) {
      this.exos = allExos["resultat"];
    }
    
    this.exoUn = allExos["resultat"][0];
    this.exoDeux = allExos["resultat"][1];
    this.exoTrois = allExos["resultat"][2];
  }
  async validateSeance() {
    let seanceEnCours = await this.myProvider.get("seancestraining/" + this.seanceId);
    let myObj = {};
    console.log(seanceEnCours);
    if ("resultat" in seanceEnCours) {
      console.log("2");
      myObj = {
        "createdAt": seanceEnCours["resultat"]["createdAt"],
        "updatedAt": seanceEnCours["resultat"]["updatedAt"],
        "id": seanceEnCours["resultat"]["id"],
        "etat": 2,//seanceEnCours["resultat"]["etat"],
        "programme": seanceEnCours["resultat"]["programme"],
        "user": seanceEnCours["resultat"]["user"],
        "nom": seanceEnCours["resultat"]["nom"]
      }
      let updateSeance = await this.myProvider.post("seancestraining/update", myObj);
      if ("resultat" in updateSeance) {
        console.log("3");
        this.nav.push(SeanceTrainPage, { id : this.navParams.get('idProgramme') });
      } 
    }
    
  }

}
